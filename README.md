# kpw-chk

Simple password checker using the [*k*-anonymity API](https://www.troyhunt.com/ive-just-launched-pwned-passwords-version-2/#cloudflareprivacyandkanonymity).

usage:

```javascript
import { isPwned } from 'kpw-chk'

async function myThingy() {
	let pw = this.getPlaintextPasswordSomehow()
	let pwned = await isPwned(pw)
}
```

If you don't trust it, you can use the sha1chk

```javascript
import { sha1IsPwned } from 'kpw-chk'

async function myThingy() {
	let pw = this.getSha1PasswordSomehow()
	let pwned = await sha1IsPwned(pw)
}
```

Other than that, there are a few properties you can make use of:

```javascript
import { cache, connections, pwnedpws_api, header_constants, getConnection } from 'kpw-chk'

console.log(cache) // Map {}
console.log(connections) // { 'https://api.pwnedpasswords.com': Http2Session {} }
console.log(pwnedpws_api) // URL { href: 'https://api.pwnedpasswords.com/', ... }
console.log(header_constants) // { ':path': '/range/', ':scheme': 'https', ':authority': 'api.pwnedpasswords.com', 'accept-encoding': 'gzip, deflate',  accept: 'text/plain' }
console.log(getConnection) // [Function: getConnection]
```

