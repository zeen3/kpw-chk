"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http2_1 = require("http2");
const url_1 = require("url");
const crypto_1 = require("crypto");
const readline_1 = require("readline");
const zlib_1 = require("zlib");
const events_1 = require("events");
const arrbuf2hex_1 = require("arrbuf2hex");
const { HTTP2_HEADER_PATH, HTTP2_HEADER_SCHEME, HTTP2_HEADER_AUTHORITY, HTTP2_HEADER_ACCEPT, HTTP2_HEADER_ACCEPT_ENCODING, HTTP2_HEADER_CONTENT_ENCODING } = http2_1.constants;
exports.pwnedpws_api = Object.preventExtensions(new url_1.URL('https://api.pwnedpasswords.com/'));
exports.header_constants = Object.preventExtensions({
    [HTTP2_HEADER_PATH]: '/range/',
    [HTTP2_HEADER_SCHEME]: 'https',
    [HTTP2_HEADER_AUTHORITY]: 'api.pwnedpasswords.com',
    [HTTP2_HEADER_ACCEPT_ENCODING]: 'gzip, deflate',
    [HTTP2_HEADER_ACCEPT]: 'text/plain',
});
exports.connections = {};
exports.getConnection = (href) => {
    let origin = href instanceof url_1.URL ? href.origin : new url_1.URL(href).origin;
    if (origin in exports.connections)
        return exports.connections[origin];
    const rm = () => {
        exports.connections[origin].close();
        exports.connections[origin].unref();
        delete exports.connections[origin];
    }, con = http2_1.connect(origin)
        .once('goaway', rm)
        .once('error', rm)
        .once('close', rm);
    exports.connections[origin] = con;
    con.setTimeout(300000, () => {
        delete exports.connections[origin];
        con.close();
    });
    return con;
};
const done = new events_1.EventEmitter;
const ongoing = {};
done.on('start', (key) => ongoing[key] = true);
done.on('end', (key) => {
    ongoing[key] = false;
    process.nextTick(done.emit.bind(done), key);
});
const onDone = (key, full) => new Promise(r => done.once(key, () => r(exports.cache.has(full) ? exports.cache.get(full) || 0 : 0)));
exports.cache = Object.preventExtensions(new Map());
const auto = (h2, cb, head) => new Promise((res, rej) => {
    h2.once('response', reshead => {
        if (reshead[':status'] !== 200)
            return process.nextTick(rej, new Error('Request did not return 200 OK'));
        if (HTTP2_HEADER_CONTENT_ENCODING in reshead)
            res(h2.pipe(zlib_1.createUnzip()).once('end', () => {
                process.nextTick(cb, 0);
                done.emit('end', head);
            }));
        else
            res(h2.once('end', () => {
                process.nextTick(cb, 0);
                done.emit('end', head);
            }));
    }).once('error', rej);
});
exports.sha1IsPwned = (digest, readOffset = 0) => {
    const header = Object.create(exports.header_constants), head = arrbuf2hex_1.arr2hex(digest, 0 + readOffset, 5, true), tail = arrbuf2hex_1.arr2hex(digest, 5 + readOffset, 35, true);
    done.emit('start', head);
    return new Promise(async (res, rej) => {
        try {
            header[HTTP2_HEADER_PATH] += head;
            readline_1.createInterface(await auto(exports.getConnection(exports.pwnedpws_api).request(header), res, head)).on('line', (line) => {
                const dg = String(line).split(':');
                if (!Array.isArray(dg))
                    return;
                const count = parseInt(dg[1], 10);
                exports.cache.set(head + dg[0], count);
                if (dg[0] === tail)
                    res(count);
            });
        }
        catch (e) {
            rej(e);
        }
    });
};
exports.isPwned = (pw, enc = 'utf8') => {
    const digest = (ArrayBuffer.isView(pw)
        ? crypto_1.createHash('SHA1').update(pw).digest()
        : crypto_1.createHash('SHA1').update(pw, enc).digest()), full = arrbuf2hex_1.arr2hex(digest, 0, 40, true), head = arrbuf2hex_1.arr2hex(digest, 0, 5, true);
    if (exports.cache.has(full))
        return Promise.resolve(exports.cache.get(full) || 0);
    else if (head in ongoing)
        return Promise.resolve(ongoing[head] ? onDone(head, full) : 0);
    else
        return exports.sha1IsPwned(digest.buffer, digest.byteOffset);
};
exports.default = exports.isPwned;
//# sourceMappingURL=kpw-chk.js.map