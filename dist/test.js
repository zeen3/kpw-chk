"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const kpw_chk_1 = require("./kpw-chk");
const assert_1 = require("assert");
const crypto_1 = require("crypto");
const nf = new Intl.NumberFormat;
const okay = (...log) => (d) => (console.log(...log, nf.format(d)), d);
(async () => {
    const pwned = kpw_chk_1.sha1IsPwned(crypto_1.createHash('SHA1').update('P@ssw0rd', 'utf8').digest()).then(okay('sha1 P@ssw0rd has %s'));
    const [P, p, n] = await Promise.all([
        kpw_chk_1.isPwned('P@ssw0rd').then(okay('isPwned P@ssw0rd has %s')),
        kpw_chk_1.isPwned('password').then(okay('isPwned password has %s')),
        kpw_chk_1.isPwned('1234').then(okay('isPwned 1234 has %s'))
    ]);
    assert_1.strictEqual(typeof await pwned, 'number', `functions return numbers`);
    assert_1.strictEqual(P !== 0, true, `"P@ssw0rd" used`);
    assert_1.strictEqual(p !== 0, true, `"password" used`);
    assert_1.strictEqual(n > 100, true, `"1234" used over 100 times`);
    console.log('"P@ssw0rd" used %s & %s times', nf.format(P), nf.format(await pwned));
    console.log('"password" used %s times', nf.format(p));
    console.log('"1234" used %s times', nf.format(n));
    let rng = crypto_1.randomBytes(6).toString('base64');
    await kpw_chk_1.isPwned(rng).then(okay('rng b64 (%s) found %s time(s)', rng));
    let rng2 = String(crypto_1.randomBytes(4).readUInt32LE(0) % 10000000);
    await kpw_chk_1.isPwned(rng2).then(okay('rng2 of %s found %s time(s)', rng2));
    let rng3 = String(crypto_1.randomBytes(4).readUInt32LE(0) % 1000000);
    await kpw_chk_1.isPwned(rng3).then(okay('rng3 of %s found %s time(s)', rng3));
    let rng4 = String(crypto_1.randomBytes(4).readUInt32LE(0) % 100000);
    await kpw_chk_1.isPwned(rng4).then(okay('rng4 of %s found %s time(s)', rng4));
    for (let i in kpw_chk_1.connections) {
        kpw_chk_1.connections[i].close();
    }
})().catch(console.error);
//# sourceMappingURL=test.js.map