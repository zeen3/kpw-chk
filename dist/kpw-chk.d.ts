/// <reference types="node" />
import { ClientHttp2Session, OutgoingHttpHeaders } from 'http2';
import { URL } from 'url';
export declare const pwnedpws_api: URL;
export declare const header_constants: OutgoingHttpHeaders;
export declare const connections: {
    [origin: string]: ClientHttp2Session;
};
export declare const getConnection: (href: string | URL) => ClientHttp2Session;
export declare const cache: Map<string, number>;
export declare const sha1IsPwned: (digest: ArrayBuffer | ArrayBufferView | SharedArrayBuffer, readOffset?: number) => Promise<number>;
export declare const isPwned: (pw: string | ArrayBufferView, enc?: "utf8" | "ascii" | "latin1") => Promise<number>;
export default isPwned;
//# sourceMappingURL=kpw-chk.d.ts.map