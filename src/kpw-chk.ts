import { connect, constants as h2_constants, ClientHttp2Session, ClientHttp2Stream, OutgoingHttpHeaders } from 'http2'
import { Duplex } from 'stream'
import { URL } from 'url'
import { createHash } from 'crypto'
import { createInterface as readLines } from 'readline'
import { createUnzip } from 'zlib'
import { EventEmitter } from 'events'
import { arr2hex } from 'arrbuf2hex'

const { HTTP2_HEADER_PATH, HTTP2_HEADER_SCHEME, HTTP2_HEADER_AUTHORITY, HTTP2_HEADER_ACCEPT, HTTP2_HEADER_ACCEPT_ENCODING, HTTP2_HEADER_CONTENT_ENCODING} = h2_constants


export const pwnedpws_api = Object.preventExtensions(new URL('https://api.pwnedpasswords.com/'))
export const header_constants:OutgoingHttpHeaders = Object.preventExtensions({
	[HTTP2_HEADER_PATH]: '/range/',
	[HTTP2_HEADER_SCHEME]: 'https',
	[HTTP2_HEADER_AUTHORITY]: 'api.pwnedpasswords.com',
	[HTTP2_HEADER_ACCEPT_ENCODING]: 'gzip, deflate',
	[HTTP2_HEADER_ACCEPT]: 'text/plain',
})
export const connections:{[origin: string]: ClientHttp2Session} = {}
export const getConnection = (href: URL | string):ClientHttp2Session => {
	let origin = href instanceof URL ? href.origin : new URL(href).origin
	if (origin in connections) return connections[origin]
	const rm = () => {
		connections[origin].close()
		connections[origin].unref()
		delete connections[origin]
	}, con = connect(origin)
		.once('goaway', rm)
		.once('error', rm)
		.once('close', rm)

	connections[origin] = con
	con.setTimeout(300_000, () => {
		delete connections[origin]
		con.close()
	})
	return con
}

const done = new EventEmitter
const ongoing:{[head: string]: boolean} = {}
done.on('start', (key: string) => ongoing[key] = true)
done.on('end', (key: string) => {
	ongoing[key] = false
	process.nextTick(done.emit.bind(done), key)
})
const onDone = (key: string, full: string) => new Promise<number>(r => done.once(key, () => r(cache.has(full) ? cache.get(full) || 0 : 0)))

export const cache = Object.preventExtensions(new Map<string, number>())
const auto = (h2: ClientHttp2Stream, cb: (v: number) => void, head: string):Promise<Duplex> => new Promise((res, rej) => {
	h2.once('response', reshead => {
		if (reshead[':status'] !== 200)
			return process.nextTick(rej, new Error('Request did not return 200 OK'))
		if (HTTP2_HEADER_CONTENT_ENCODING in reshead)
			res(
				h2.pipe(createUnzip()).once('end', () => {
					process.nextTick(cb, 0)
					done.emit('end', head)
				})
			)
		else res(
			h2.once('end', () => {
				process.nextTick(cb, 0)
				done.emit('end', head)
			})
		)
	}).once('error', rej)
	
})
export const sha1IsPwned = (digest: ArrayBuffer | SharedArrayBuffer | ArrayBufferView, readOffset: number = 0):Promise<number> => {
	const header = Object.create(header_constants),
		head = arr2hex(digest, 0 + readOffset, 5, true),
		tail = arr2hex(digest, 5 + readOffset, 35, true)

	done.emit('start', head)
	return new Promise<number>(async (res, rej) => {
		try {
			header[HTTP2_HEADER_PATH] += head
			// console.log(header, Object.getPrototypeOf(header))
			// header is extensible; constant header is inextensible
			readLines(
				await auto(
					getConnection(pwnedpws_api).request(header),
					res,
					head
				)
			).on('line', (line:string) => {
				const dg = String(line).split(':')
				if (!Array.isArray(dg)) return
				const count = parseInt(dg[1], 10)
				cache.set(head + dg[0], count)
				if (dg[0] === tail) res(count)
			})
		} catch (e) {rej(e)}
	})
}
export const isPwned = (pw: string | ArrayBufferView, enc: 'utf8' | 'ascii' | 'latin1' = 'utf8'):Promise<number> => {
	const digest = (
		ArrayBuffer.isView(pw)
		? createHash('SHA1').update(pw).digest()
		: createHash('SHA1').update(pw, enc).digest()
	),
		full = arr2hex(digest, 0, 40, true),
		head = arr2hex(digest, 0, 5, true)

	if (cache.has(full)) return Promise.resolve(cache.get(full) || 0)
	else if (head in ongoing) return Promise.resolve(ongoing[head] ? onDone(head, full) : 0)
	else return sha1IsPwned(digest.buffer, digest.byteOffset)
}
export default isPwned

