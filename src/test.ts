import { isPwned, sha1IsPwned, connections } from './kpw-chk'
import { strictEqual } from 'assert'
import { createHash, randomBytes } from 'crypto'

const nf = new Intl.NumberFormat
const okay = (...log: string[]) => (d: number) => (console.log(...log, nf.format(d)), d);
(async () => {
	const pwned = sha1IsPwned(createHash('SHA1').update('P@ssw0rd', 'utf8').digest()).then(okay('sha1 P@ssw0rd has %s'))
	const [P, p, n] = await Promise.all([
		isPwned('P@ssw0rd').then(okay('isPwned P@ssw0rd has %s')),
		isPwned('password').then(okay('isPwned password has %s')),
		isPwned('1234').then(okay('isPwned 1234 has %s'))
	])
	strictEqual(typeof await pwned, 'number', `functions return numbers`)
	strictEqual(P !== 0, true, `"P@ssw0rd" used`)
	strictEqual(p !== 0, true, `"password" used`)
	strictEqual(n > 100, true, `"1234" used over 100 times`)
	console.log('"P@ssw0rd" used %s & %s times', nf.format(P), nf.format(await pwned))
	console.log('"password" used %s times', nf.format(p))
	console.log('"1234" used %s times', nf.format(n))
	let rng = randomBytes(6).toString('base64')
	await isPwned(rng).then(okay('rng b64 (%s) found %s time(s)', rng))
	let rng2 = String(randomBytes(4).readUInt32LE(0) % 10_000_000)
	await isPwned(rng2).then(okay('rng2 of %s found %s time(s)', rng2))
	let rng3 = String(randomBytes(4).readUInt32LE(0) % 1_000_000)
	await isPwned(rng3).then(okay('rng3 of %s found %s time(s)', rng3))
	let rng4 = String(randomBytes(4).readUInt32LE(0) % 100_000)
	await isPwned(rng4).then(okay('rng4 of %s found %s time(s)', rng4))
	for (let i in connections) {
		connections[i].close()
	}
})().catch(console.error)
